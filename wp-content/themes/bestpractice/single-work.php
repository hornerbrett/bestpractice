<?php
/*
 * Template Name: Single Work Template
 */
?>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.theme.default.min.css">
<?php get_header(); ?>

<?php 
	if(isset($_GET['cat'])) { 
		$cat = $_GET['cat'];
	}
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
	<?php 
	$images = get_field('slideshow');
	$size = 'full'; // (thumbnail, medium, large, full or custom size)
	if( $images ): ?>
	<div id="slideshow">
		<div class="owl-carousel loop">
	        <?php foreach( $images as $image ): ?>
	            <div class="img-container">
		            <p class="credit"><?php echo $image['caption'];?></p>	
	                <img src="<?php echo $image['url']; ?>">
	            </div>
	        <?php endforeach; ?>
		</div>
	</div>	
	<?php endif; ?>

<section id="project">
	<div id="info">
		<h1 id="page-title" class="text-center"><?php the_title();?></h1>	
		
		<div class="width-40 pull-left ">
			<div class="content pull-right">
				<?php the_field('content');?>
<!--
				<h4>Categories</h4>	
				<?php 
				$categories = get_the_terms(get_the_id(), 'work-categories');
			    foreach($categories as $category) {
			        echo $category->name . ", ";
			    }
			    ?>
			    <?php $term = get_term_by('slug', $cat, 'work-categories'); $name = $term->term_id; ?>
			    <?php echo $name;?>
-->
			</div>	
		</div>	
		<div class="width-60 pull-left main-img">
			<?php $image = get_field('featured_image'); ?>
			<img src="<?php echo $image['url']; ?>">
		</div>
	</div>
	
	<div class="container">
			
			<?php
			
			// Check value exists.
			if( have_rows('blocks') ):
			
			    // Loop through rows.
			    while ( have_rows('blocks') ) : the_row();
			
			        if( get_row_layout() == 'image' ): ?>
			        <?php $image = get_sub_field('image'); ?>
			        <?php $caption = get_field('caption'); ?>
					<div class="block image <?php if(get_sub_field('full_width') == 'true') { echo 'full'; }?>">
						<div class="img-container">
							<p class="credit"><?php the_sub_field('caption');?></p>	
							<img class="animate" src="<?php echo $image['url'];?>">
						</div>	
						<?php if( $caption ): ?>
					        <p class="caption"><?php echo $caption;?></p>
					    <?php endif; ?>
					</div>	
			        <?php elseif( get_row_layout() == 'text' ): ?>
					<div class="text block content text-center full">
						<?php the_sub_field('text'); ?>	
					</div>	
			        <?php elseif( get_row_layout() == 'before__after' ): ?>
			        <?php $before = get_sub_field('before_image'); ?>
			        <?php $after = get_sub_field('after_image'); ?>
					<div class="block full">
						<div class="ba-slider">
						  <img src="<?php echo $before['url'];?>" alt="">       
						  <div class="resize">
						    <img src="<?php echo $after['url'];?>" alt="">
						  </div>
						  <span class="handle"></span>
						</div>
					</div>	
			        <?php elseif( get_row_layout() == 'quote' ): ?>
			        <div class="block quote">
						<div class="quote-inner">
							<div class="quotes">
								<?php include get_template_directory() . '/img/quotes.svg'; ?>
							</div>	
							<p><?php the_sub_field('quote'); ?>
							<br><span><?php the_sub_field('quoted_by'); ?></span></p>	
						</div>	
					</div>	
			        <?php elseif( get_row_layout() == 'video' ): ?>
			        <?php $image = get_sub_field('poster'); ?>
			        <?php $link = get_sub_field('link'); ?>
			        <?php $link = str_replace("watch?v=", "embed/", $link); ?>
			        <div class="block video full">
						<a class="vid animate" href="#">
							<div class="corner outs"></div>	
							<div class="play outs"><?php include get_template_directory() . '/img/play.svg'; ?></div>	
							<img class="outs" src="<?php echo $image['url'];?>">
							
							<div class="video-container">
<!-- 								<iframe width="560" height="315" src="<?php echo $link; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
								<?php the_sub_field('vid');?>
							</div>		
	
						</a>						
					</div>
			        <?php elseif( get_row_layout() == 'image__text' ): ?>
			        <?php $image = get_sub_field('image'); ?>
			        <section class="block text-image">
						<div class="right-image">
							<img class="animate" src="<?php echo $image['url'];?>">
						</div>	
						<div class="left-text text-center width-50 pull-left">
							<div class="inner pull-right text">
								<?php the_sub_field('text'); ?>
							</div>	
						</div>	
					</section>	
			        
			        <?php endif;
			
			    // End loop.
			    endwhile;
			
			// No value.
			else :
			    // Do something...
			endif;
			?>
			
	</div>	
	
	<div class="clearfix"></div>	
	
	<div id="page-links">
		
		<?php
		$terms = get_terms( array(
		    'taxonomy' => 'work-categories',
		    'hide_empty' => false,
		) );
		
		foreach($terms as $term) {
			$cats[] = $term->term_id;
		}
		$del_val = $name;
		if (($key = array_search($del_val, $cats)) !== false) {
		    unset($cats[$key]);
		}
	    ?>
		
		<div class="next-link">
			<?php next_post_link('%link', 'Previous Project', true, '', 'work-categories'); ?>			
		</div>	
		<div class="prev-link">
			<?php previous_post_link('%link', 'Next Project', true, '', 'work-categories'); ?>
		</div>	
	</div>	
	
</section>	

<?php endwhile; 
endif; 
?>


<script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/owl.autoplay.js"></script>

<script>
	$(document).ready(function(){
		singleWork();	
		$('#slideshow').addClass('loaded');
			
		// Call & init
		$(document).ready(function(){
		  $('.ba-slider').each(function(){
		    var cur = $(this);
		    // Adjust the slider
		    var width = cur.width()+'px';
		    cur.find('.resize img').css('width', width);
		    // Bind dragging events
		    drags(cur.find('.handle'), cur.find('.resize'), cur);
		  });
		});
		
		// Update sliders on resize. 
		// Because we all do this: i.imgur.com/YkbaV.gif
		$(window).resize(function(){
		  $('.ba-slider').each(function(){
		    var cur = $(this);
		    var width = cur.width()+'px';
		    cur.find('.resize img').css('width', width);
		  });
		});
		
		function drags(dragElement, resizeElement, container) {
			
		  // Initialize the dragging event on mousedown.
		  dragElement.on('mousedown touchstart', function(e) {
		    
		    dragElement.addClass('draggable');
		    resizeElement.addClass('resizable');
		    
		    // Check if it's a mouse or touch event and pass along the correct value
		    var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;
		    
		    // Get the initial position
		    var dragWidth = dragElement.outerWidth(),
		        posX = dragElement.offset().left + dragWidth - startX,
		        containerOffset = container.offset().left,
		        containerWidth = container.outerWidth();
		 
		    // Set limits
		    minLeft = containerOffset + 10;
		    maxLeft = containerOffset + containerWidth - dragWidth - 10;
		    
		    // Calculate the dragging distance on mousemove.
		    dragElement.parents().on("mousemove touchmove", function(e) {
		    	
		      // Check if it's a mouse or touch event and pass along the correct value
		      var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;
		      
		      leftValue = moveX + posX - dragWidth;
		      
		      // Prevent going off limits
		      if ( leftValue < minLeft) {
		        leftValue = minLeft;
		      } else if (leftValue > maxLeft) {
		        leftValue = maxLeft;
		      }
		      
		      // Translate the handle's left value to masked divs width.
		      widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';
					
		      // Set the new values for the slider and the handle. 
		      // Bind mouseup events to stop dragging.
		      $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function () {
		        $(this).removeClass('draggable');
		        resizeElement.removeClass('resizable');
		      });
		      $('.resizable').css('width', widthValue);
		    }).on('mouseup touchend touchcancel', function(){
		      dragElement.removeClass('draggable');
		      resizeElement.removeClass('resizable');
		    });
		    e.preventDefault();
		  }).on('mouseup touchend touchcancel', function(e){
		    dragElement.removeClass('draggable');
		    resizeElement.removeClass('resizable');
		  });
		}


/*
		$('.blocks').isotope({
			itemSelector: '.block',
			percentPosition: true,
			masonry: {
			// use outer width of grid-sizer for columnWidth
			columnWidth: '.grid-sizer'
			}
		})
*/
		$('a.vid').click(function(e){
			e.preventDefault();
			$(this).addClass('gotit').find('.outs').addClass('gone');
		});
		
	});
</script>	

<?php get_footer(); ?>