<?php
/*
 * Template Name: Default Page Template
 */
?>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.theme.default.min.css">
<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
<section id="blog-post">
	
	<div class="content-container container small-container">
		<div class="content text">
			<?php if(get_field('featured_image')) { ?>
			<div class="post-img">
				<?php $image = get_field('featured_image');?>
				<img src="<?php echo $image['url'];?>">
			</div>	
			<?php } ?>
			<h1><?php the_title();?></h1>	
			<?php the_content();?>
		</div>	
	</div>	
		
	<div class="clearfix"></div>	
	
	<div id="page-links">
		<div class="prev-link">
			<?php previous_post_link('%link', 'Previous Post'); ?>
		</div>	
		<div class="next-link">
			<?php next_post_link('%link', 'Next Post'); ?>			
		</div>	
	</div>	
	
</section>	

<?php endwhile; 
endif; 
?>


<script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/owl.autoplay.js"></script>

<script>
	$(document).ready(function(){
		singleWork();		
		

/*
		$('.blocks').isotope({
			itemSelector: '.block',
			percentPosition: true,
			masonry: {
			// use outer width of grid-sizer for columnWidth
			columnWidth: '.grid-sizer'
			}
		})
*/

		
	});
</script>	

<?php get_footer(); ?>