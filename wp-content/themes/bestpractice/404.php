<?php get_header(); ?>

<div class="container small-container">
	<h1>Oops!</h1>	
	<p>This is somewhat embarrassing, isn't it? It looks like nothing was found at this location.</p>	
</div>	

<?php get_footer(); ?>
