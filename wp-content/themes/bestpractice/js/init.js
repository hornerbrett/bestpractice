$(function(){
	
	header();
	animations();
	
	$windowWidth = $(window).outerWidth();
	
	if($windowWidth > 991) {
		var s = skrollr.init();
	}
	
	$(window).resize(function(){
		//
	}).triggerHandler('resize');
	
});	

function animations() {
	
	$('.animate').on('inview', function(event, visible) {
		if(visible) {
			$(this).addClass('in-view');
		}
	});
	
}

function header() {
	$(window).scroll(function() {
	    var height = $(window).scrollTop();
	    if(height  > 60) {
	        $('body').addClass('scrolled').removeClass('top-page');
	    } else {
	        $('body').removeClass('open').addClass('top-page');
	    }
	});

	$('a#toggle').click(function(e) {
		e.preventDefault();
		$('body').toggleClass('open');
	});
	
	$('html').click(function() {
		$('body').removeClass('open');
	});
	
	$('#nav, a#toggle').click(function(event){
	    event.stopPropagation();
	});
	
	$('html').on('touchstart', function(e) {
		$('body').removeClass('open');
	});
			
	$('#nav, a#toggle').on('touchstart',function(e) {
	    e.stopPropagation();
	});
	
	$('a.close').click(function(){
		$('body').removeClass('alerted');
	});
}

function work() {
	
	$('.work .grid').imagesLoaded(function() {
		$('.work .grid').isotope({
			itemSelector: '.item',
			percentPosition: true,
			masonry: {
				columnWidth: '.item'
			}
		});
		
		$('.work .grid').on('layoutComplete', function(event, laidOutItems) {
			$('.work .grid').addClass('visible');
		});
	});
	
	$('.filters').on('click', 'a', function(e) {
		var filter = $(this).attr('data-filter');
		$('.filters li').removeClass('active');
		$(this).parent().addClass('active');
		$('.work .grid').isotope({
			filter: filter
		});
	});
	
	var page_identifier = $('.work .page-identifier').text();
	if(page_identifier == 'Work') {
		$('.filters li').first().find('a').trigger('click');
	} else {
		$('.filters li a[data-filter=".' + page_identifier + '"]').trigger('click');
	}
	
	if(window.location.hash) {
		var link = window.location.hash.substring(1);
		$('a[href="#filters"]').trigger('click');
		$('.filters li a[data-filter=".' + link + '"]').trigger('click');
		
/*
		$('.work-thumb').each(function() {
			$hash = window.location.hash.substring(1);
			_href = $(this).attr("href");
			
			$(this).attr("href", _href + '?cat=' + $hash);
		});
*/
		
	}
	
/*
	$filterOffset = $('#stickit').offset().top - 91;
	
	$(window).scroll(function() {
		
		$windowScroll = $(window).scrollTop();
		
		console.log($filterOffset);
		
		if($windowScroll > $filterOffset ) {
			$('body').addClass('stuck');
			$dHeight = $('#stickit').outerHeight();
			$('.filler').height($dHeight);
		} else {
			$('body').removeClass('stuck');
		}
	});
*/
	
}

function studio() {
	
/*
	$filterOffset = $('#page-nav').offset().top - 91;
	
	$(window).scroll(function() {
		
		$windowScroll = $(window).scrollTop();
		
		console.log($filterOffset);
		
		if($windowScroll > $filterOffset ) {
			$('body').addClass('stuck');
			$dHeight = $('#page-nav').outerHeight();
			$('.filler').height($dHeight);
		} else {
			$('body').removeClass('stuck');
		}
	});
*/
	
	$('.person').click(function() {
	    
		if(!$(this).hasClass('active')) {
			var person_information = $(this).closest('.person:visible').find('.full-information').html(),						
				expanded_information = $('<div class="column full-information left width-100 open" />'),
				top_position = $(this).closest('.person:visible').position().top,
				index_position = $(this).closest('.person:visible').index('.person:visible'),
				total_people = $('#team .mix-container .person:visible').length - 1;
				
			console.log(top_position);
				
			$('#team .mix-container .person:visible').removeClass('active').addClass('non-active');
			
			$(this).removeClass('non-active').addClass('active');	
			
			expanded_information.append(person_information);
			
			$('#team .mix-container .column.full-information').removeClass('open').addClass('remove');
			
			for(var i = index_position; i <= total_people; i++) {
				if(top_position < $('#team .mix-container .person:visible').eq(i).position().top) {
					$(expanded_information).insertBefore($('#team .mix-container .person:visible').eq(i));
					break;
				}
				if(i == total_people) {
					$(expanded_information).appendTo($('#team .mix-container'));
					break;
				}
			}
			
			$('#team .mix-container .column.full-information.remove').slideUp(125, function() {
				$('#team .mix-container .column.full-information.remove').remove();
			});
			
			expanded_information.slideDown(125, function() {
				$('#team .mix-container .column.full-information').addClass('active');
				
				$('html,body').animate({
		          scrollTop: $('.full-information.active').offset().top - 240
		        }, 500);
				
			});
			
			
		} else {
			$(this).removeClass('active').addClass('no-active');
			$('#team .mix-container .column.full-information').removeClass('125').addClass('remove');
			$('#team .mix-container .column.full-information.remove').slideUp(125, function() {
				$('#team .mix-container .column.full-information.remove').remove();
			});
		}
		return false;
	});
	
	$('#page-nav li a').click(function(e) {
		
		$('#page-nav li').removeClass('active');
		$(this).parent('li').addClass('active');
		
		var targetHref = $(this).attr('href');
		
		$('html, body').animate({
			scrollTop: $(targetHref).offset().top - 140
		}, 1000);
		
		e.preventDefault();
	});

	  /*------------------------------------------------------
	  adds active class to the nav element where the scroll position is currently at
	  ------------------------------------------------------*/
	  $(window).scroll(function() {
	    var scrollPosition = $(window).scrollTop();
	    
	    $("#page-nav li").each(function(index) {
			$id = $(this).data('nav');
			$divTop = $($id).offset().top;
			
			if ((scrollPosition + 180) >= $divTop) {
				$("#page-nav li").removeClass('active');
				$('#page-nav li[data-nav="' + $id + '"]').addClass('active');
			}
			
		});
	        

	  });
	
}

function singleWork() {
	$('.loop').owlCarousel({
	    center: true,
	    items:3,
	    loop:true,
	    margin:50,
	    nav: true,
	    autoWidth:true,
/*
	    autoplay : true,
		slideTransition: 'linear',
		autoplayTimeout : 5000,
		autoplayHoverPause : false,
		autoplaySpeed : 5000,
*/
	    responsive:{
			1400: {
				margin:70,
		    },
		    1680: {
				margin:100,
		    },
	    }
	});
}

function news() {
	$('.grid').imagesLoaded(function() {
		$('.grid').isotope({
			itemSelector: '.grid .column',
			percentPosition: true,
			masonry: {
				columnWidth: '.grid .column'
			}
		});
	});	
}