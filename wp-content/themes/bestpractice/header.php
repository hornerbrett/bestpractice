<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    
    <link rel='shortcut icon' type='image/x-icon' href='<?php bloginfo('template_url'); ?>/img/favicon.png' />
    <meta name="author" content="DEI Creative in Seattle, WA" />
  	<meta name="keywords" content="" />
  	<meta name="description" content="" />
  	<meta name="robots" content="all" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<title><?php wp_title('&#124;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_get_archives(array('type' => 'monthly', 'format' => 'link')); ?>
	
	<script type="text/javascript">
		var theme = '<?php echo get_template_directory_uri(); ?>';
	</script>
	
	<?php wp_head(); ?>
	
</head>

<?php if(get_field('alert', 'options')) { ?>
	<body <?php body_class('top-page alerted');?>>
<?php } else { ?>
	<body <?php body_class('top-page');?>>
<?php } ?>
	<header>
		<?php if ( is_front_page() && get_field('alert', 'options') ) : ?>
		<div class="alert">
			<?php the_field('alert', 'options');?>
			<a class="close" href="#"></a>	
		</div>	
		<?php endif;?>
		<div id="header-inner">
			
			<?php if ( is_front_page() ) { ?>
				<a id="logo" href="<?php echo site_url();?>"></a>	
			<?php } else { ?>
				<a id="new-logo" href="<?php echo site_url();?>"><?php include get_template_directory() . '/img/new-logo.svg'; ?></a>	
			<?php } ?>
			
			<ul id="nav" class="menu">
				<li class="menu-item work-menu"><a href="<?php echo site_url();?>/work/">Work</a></li>
				<?php
				$terms = get_terms( array(
				    'taxonomy' => 'work-categories',
				    'hide_empty' => false,
				) );
				
				foreach($terms as $term) {
				?>
					<li class="sub-item"><a href="<?php echo site_url();?>/work/#<?php echo $term->slug;?>" data-filter=".<?php echo $term->slug;?>"><?php echo $term->name;?></a></li>	
				<?php
				}
	
			    ?>
								
				<?php
					$defaults = array(
						'echo'            => true,
						'menu'			  => 'Nav',
						'menu_id'		  => 'nav',
						'items_wrap' => '%3$s'
					);
					
					wp_nav_menu( $defaults );
				?>
			</ul>
			
			<a id="toggle" href="#">
				<span class="top"></span>	
				<span class="bottom"></span>	
			</a>	
		</div>
	</header>	
	<div id="header-spacer"></div>	