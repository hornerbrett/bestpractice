<?php
/*
 * Template Name: Single Post Template
 */
?>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.theme.default.min.css">
<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
	<?php 
	$images = get_field('slideshow');
	$size = 'full'; // (thumbnail, medium, large, full or custom size)
	if( $images ): ?>
	<div id="slideshow">
		<div class="owl-carousel loop">
	        <?php foreach( $images as $image ): ?>
	            <div>
	                <img src="<?php echo $image['url']; ?>">
	            </div>
	        <?php endforeach; ?>
		</div>
	</div>	
	<?php endif; ?>

<section id="project">
	<div id="info">
		<h1 id="page-title" class="text-center"><?php the_title();?></h1>	
		
		<div class="width-40 pull-left <?php if(!get_field('featured_image')) { echo 'no-image'; } ?>">
			<div class="content pull-right">
				<?php the_field('content');?>
			</div>	
		</div>	
		<?php if(get_field('featured_image')) { ?>
		<div class="width-60 pull-left main-img">
			<?php $image = get_field('featured_image'); ?>
			<img src="<?php echo $image['url']; ?>">
		</div>
		<?php } ?>
	</div>
	
	<div class="container">
			
			<?php
			
			// Check value exists.
			if( have_rows('blocks') ):
			
			    // Loop through rows.
			    while ( have_rows('blocks') ) : the_row();
			
			        if( get_row_layout() == 'image' ): ?>
			        <?php $image = get_sub_field('image'); ?>
			        <?php $caption = $image['caption']; ?>
					<div class="block image <?php if(get_sub_field('full_width') == 'true') { echo 'full'; }?>">
						<img class="animate" src="<?php echo $image['url'];?>">
						<?php if( $caption ): ?>
					        <p class="caption"><?php echo $caption;?></p>
					    <?php endif; ?>
					</div>	
			        <?php elseif( get_row_layout() == 'text' ): ?>
					<div class="text block content text-center full">
						<?php the_sub_field('text'); ?>	
					</div>	
			        <?php elseif( get_row_layout() == 'quote' ): ?>
			        <div class="block quote">
						<div class="quote-inner">
							<div class="quotes">
								<?php include get_template_directory() . '/img/quotes.svg'; ?>
							</div>	
							<p><?php the_sub_field('quote'); ?>
							<br><span><?php the_sub_field('quoted_by'); ?></span></p>	
						</div>	
					</div>	
			        <?php elseif( get_row_layout() == 'video' ): ?>
			        <?php $image = get_sub_field('poster'); ?>
			        <div class="block video full">
						<a class="vid animate" href="<?php the_sub_field('link'); ?>" data-lity>
							<div class="corner"></div>	
							<div class="play"><?php include get_template_directory() . '/img/play.svg'; ?></div>	
							<img class="" src="<?php echo $image['url'];?>">
						</a>
					</div>
			        <?php elseif( get_row_layout() == 'image__text' ): ?>
			        <?php $image = get_sub_field('image'); ?>
			        <section class="block text-image">
						<div class="right-image">
							<img class="animate" src="<?php echo $image['url'];?>">
						</div>	
						<div class="left-text text-center width-50 pull-left">
							<div class="inner pull-right text">
								<?php the_sub_field('text'); ?>
							</div>	
						</div>	
					</section>	
			        
			        <?php endif;
			
			    // End loop.
			    endwhile;
			
			// No value.
			else :
			    // Do something...
			endif;
			?>
			
	</div>	
	
	<div class="clearfix"></div>	
	
	<div id="page-links">
		<div class="prev-link">
			<?php previous_post_link('%link', 'Previous Post'); ?>
		</div>	
		<div class="next-link">
			<?php next_post_link('%link', 'Next Post'); ?>			
		</div>	
	</div>	
	
</section>	

<?php endwhile; 
endif; 
?>


<script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/owl.autoplay.js"></script>

<script>
	$(document).ready(function(){
		singleWork();		
		

/*
		$('.blocks').isotope({
			itemSelector: '.block',
			percentPosition: true,
			masonry: {
			// use outer width of grid-sizer for columnWidth
			columnWidth: '.grid-sizer'
			}
		})
*/

		
	});
</script>	

<?php get_footer(); ?>