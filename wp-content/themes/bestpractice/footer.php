<?php wp_footer(); ?>

<footer>
	<div class="container">
		<div class="width-33 column pull-left">
			<ul class="social">
				<?php if(get_field('facebook', 'options')) {?><li class="facebook"><a target="_blank" href="<?php the_field('facebook', 'options');?>"><?php include get_template_directory() . '/img/social/facebook.svg'; ?></a></li><?php } ?>
				
				<?php if(get_field('instagram', 'options')) {?><li class="instagram"><a target="_blank" href="<?php the_field('instagram', 'options');?>"><?php include get_template_directory() . '/img/social/instagram.svg'; ?></a></li><?php } ?>

				<?php if(get_field('linkedin', 'options')) {?><li class="linkedin"><a target="_blank" href="<?php the_field('linkedin', 'options');?>"><?php include get_template_directory() . '/img/social/linkedin.svg'; ?></a></li><?php } ?>
				
			</ul>	
			<?php the_field('terms_link', 'options');?>
		</div>	
		<div class="width-33 column pull-left text-right">
			<a target="_blank" href="https://www.google.com/maps/dir//Best+Practice,+<?php the_field('address', 'options');?>"><?php the_field('address', 'options');?></a>
			<a href="tel:<?php the_field('phone', 'options');?>"><?php the_field('phone', 'options');?></a>
		</div>	
		<div class="width-33 column pull-left text-right">
			<p>© Best Practice Architecture, All rights reserved.<br>
	Site Design by <a href="https://deicreative.com" target="_blank" class="inline">DEI Creative</a></p>	
		</div>	
	</div>	
</footer>	

<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.6.5/lottie.min.js'></script>

<script>
	var animation = bodymovin.loadAnimation({
		container: document.getElementById('logo'),
		renderer: 'svg',
		loop: false,
		autoplay: true,
		path: '<?php bloginfo('template_url'); ?>/js/bp_foldin.json'
	})
</script>	

</body>
</html>