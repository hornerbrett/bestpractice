<?php
/*
 * Template Name: Work Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div class="container">	
	
	<div class="intro text-center">
		<?php the_field('intro');?>
	</div>	
	
	<div class="filler"></div>
	<div id="stickit">
		<ul id="filters" class="filters">
			<li><a href="#" data-filter="*" class="highlight no-underline">All</a></li>	

<!--
			<li><a href="#" data-filter=".red">Restaurants</a></li>	
			<li><a href="#" data-filter=".blue">Retail</a></li>	
			<li><a href="#" data-filter=".green">Office</a></li>	
			<li><a href="#" data-filter=".yellow">Residential</a></li>	
-->
			
			<?php 

		    $terms = get_terms( array(
			    'taxonomy' => 'work-categories',
			    'hide_empty' => false,
			) );
			
			foreach($terms as $term) {
			?>
				<li><a href="#<?php echo $term->slug;?>" data-filter=".<?php echo $term->slug;?>"><?php echo $term->name;?></a></li>	
			<?php
			}

		    ?>
							
		</ul>
	</div>		
	
	<section id="projects" class="work">
		<div class="grid center">
			
		<?php
		$args = array(
			'post_type' => 'work',
			'orderby' => 'date',
		    'order'   => 'DESC',
		    'posts_per_page' => -1
		);
		// The Query
		$query = new WP_Query( $args );
		 
		// The Loop
		if ( $query->have_posts() ) {
		    while ( $query->have_posts() ) {
		        $query->the_post();
		?>
		
    	<div class="left-image item 
			<?php 
			$categories = get_the_terms(get_the_id(), 'work-categories');
		    foreach($categories as $category) {
		        echo $category->slug . " ";
		    }
		    ?>
		">
				

			<a href="<?php the_permalink();?>" class="work-thumb">
				<?php $image = get_field('work_page_image');?>
				<div class="inner"><img class="animate" src="<?php echo $image['url'];?>"></div>	
			</a>	
			<p><?php the_title();?></p>	

			 
		</div>
		
		<?php
		    }
		} else {
		    // no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();
		?>

			
				
		</div>
	</section>	

</div>

<script>
	$(document).ready(function(){
		work();		
	});
</script>	

<?php get_footer(); ?>