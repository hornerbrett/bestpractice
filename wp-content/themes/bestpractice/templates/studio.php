<?php
/*
 * Template Name: Studio Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div class="container">	
	
	<div class="intro text-center">
		<?php the_field('about_intro');?>
	</div>	
	
	<div class="filler">
		<div id="stickit">
			<ul id="page-nav">
				<li data-nav="#about"><a href="#about">About</a></li>	
				<li data-nav="#team"><a href="#team">Team</a></li>	
				<li data-nav="#awards"><a href="#awards">Awards</a></li>	
				<li data-nav="#press"><a href="#press">Press</a></li>	
			</ul>
		</div>	
	</div>	
	
	<section id="about">
		<section class="block text-image">
			<div class="left-text text-center width-50 pull-left">
				<div class="inner pull-right text">
					<?php the_field('section_1_text');?>
				</div>	
			</div>	
			<div class="right-image">
				<?php $image = get_field('section_1_image');?>
				<img class="animate" src="<?php echo $image['url'];?>">
			</div>	
		</section>	
		
		<section class="block text-image inverse">
			<div class="left-image">
				<a class="img-outer" href="<?php the_field('instagram','options');?>" target="_blank">
					<div class="corner"></div>	
					<div class="play"><?php include get_template_directory() . '/img/instagram.svg'; ?></div>
					<?php $image = get_field('section_2_image');?>
					<img class="animate" src="<?php echo $image['url'];?>">
				</a>	
				<p class="caption"><?php echo $image['caption'];?></p>	

			</div>	
			<div class="left-text text-center width-50 pull-left">
				<div class="inner pull-right text">
					<?php the_field('section_2_text');?>
				</div>	
			</div>	
		</section>	
		
		<section class="block text-image">
			<div class="left-text text-center width-50 pull-left">
				<div class="inner pull-right text">
					<?php the_field('section_3_text');?>
				</div>	
			</div>	
			<div class="right-image">
				<?php $image = get_field('section_3_image');?>
				<img class="animate" src="<?php echo $image['url'];?>">
			</div>	
		</section>	
	</section>	
	
	<section id="team">
		<div class="intro text-center">
			<?php the_field('team_intro');?>
		</div>	
		
		<div class="mix-container">
			
			<?php
			$i = 1;
			// check if the repeater field has rows of data
			if( have_rows('team') ):
			
			 	// loop through the rows of data
			    while ( have_rows('team') ) : the_row();
			    $image = get_sub_field('headshot');
			    $image_hover = get_sub_field('headshot_hover');
			?>
			
			<div class="mix person">
				<div class="mix-inside">
				    <div class="mix-img">
					   	<img class="illo" src="<?php echo $image['url'];?>">
					   	<div class="hover"><img src="<?php echo $image_hover['url'];?>"></div>	
				    </div>	
					
				    <div class="mix-text">
						<h4 class=""><?php the_sub_field('name'); ?></h4>
						<p class="job"><?php the_sub_field('position'); ?></p>
						<p class="view">View Bio</p>	
				    </div>	
				    
				    <div class="full-information">
			            <div class="inside">
			            	<div class="text" id="info<?php echo $i; ?>">	
				            	<div class="container small-container text-center">
									<?php the_sub_field('bio'); ?>									
				            	</div>	
				            </div>
			            </div>
			        </div>
				</div>
				<?php $i++;?>
			</div>
			
			<?php
			    endwhile;
			
			else :
			
			    // no rows found
			
			endif;
			
			?>
											
		</div>
		
	</section>
	
	<section id="awards" class="text-center text">
		<?php the_field('awards');?>
	</section>	
	
	<section id="press" class="text-center  text">
		<?php the_field('press');?>
	</section>		
	
</div>

<script>
	$(document).ready(function(){
		studio();
	});
</script>	

<?php get_footer(); ?>