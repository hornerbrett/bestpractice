<?php
/*
 * Template Name: Contact Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div class="container">	
	<section class="text-image contact">
		<div class="left-text text-center width-50 pull-left">
			<div class="inner pull-right text">
				<h1><?php the_field('title');?></h1>
				<p><a href="tel:<?php the_field('phone');?>"><?php the_field('phone');?></a></p>	
				<p><a href="mailto:<?php the_field('email');?>"><?php the_field('email');?></a></p>	
				<p><a target="_blank" href="https://www.google.com/maps/dir//Best+Practice,+<?php echo str_replace('<br />', '', get_field('address'));?>"><?php the_field('address');?></a></p>
				<a class="link" target="_blank" href="https://www.google.com/maps/dir//Best+Practice,+<?php echo str_replace('<br />', '', get_field('address'));?>">Directions</a>
			</div>	
		</div>	
		<div class="right-image map">
			<div id="map" style="height:800px;">
				<div class="marker" data-lat="47.598990" data-lng="-122.313180"></div>
			</div>	
<!--
			<a target="_blank" href="https://www.google.com/maps/dir//Best+Practice,+<?php echo str_replace('<br />', '', get_field('address'));?>">
				<img class="animate" src="<?php bloginfo('template_url'); ?>/img/map.jpg">
			</a>
-->
		</div>	
	</section>	
</div>

<section id="band" class="bg-blue text-center">
	<div class="container">
		<h1><?php the_field('join_title');?></h1>	
		<p><?php the_field('join_text');?></p>
		<a class="link inline-block" href="<?php the_field('link');?>">Learn More</a>		
	</div>	
</section>	

<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?v=3.exp&#038;key=AIzaSyD88U0r_2iwFkgR6ZHTI1Rw72OG_FTU1mQ&#038;libraries=geometry&#038;sensor=false&#038;ver=5.3.2'></script>

<script>
	function initMap($el) {
	    var $markers = $el.find('.marker');
	    var mapArgs = {
	        zoom : $el.data('zoom') || 14,
	        mapTypeId : google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			zoomControl: true,
			draggable: true,
			scrollwheel: false,
	        styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#47b9c7"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
	    };
	    var map = new google.maps.Map($el[0], mapArgs);
	    map.markers = [];
	    $markers.each(function(){
	        initMarker($(this), map);
	    });
	    centerMap(map);
	    return map;
	}
	function initMarker($marker, map) {
	    var lat = $marker.data('lat');
	    var lng = $marker.data('lng');
	    var latLng = {
	        lat: parseFloat(lat),
	        lng: parseFloat(lng)
	    };
	    var image = {
			url: theme + '/img/marker.svg', 
			scaledSize: new google.maps.Size(32, 32), 
			origin: new google.maps.Point(0, 0), 
			anchor: new google.maps.Point(16, 16)
		};
		var image_hover = {
			url: theme + '/img/contact/marker-hover.svg', 
			scaledSize: new google.maps.Size(32, 32), 
			origin: new google.maps.Point(0, 0), 
			anchor: new google.maps.Point(16, 16)
		};
	    var marker = new google.maps.Marker({
	        position : latLng,
	        icon: image,
	        map: map
	    });
	    map.markers.push(marker);
		marker.addListener('click', function() {
			window.open('https://www.google.com/maps/place/' + marker.position);
        });
		google.maps.event.addListener(marker, 'mouseover', function() {
			marker.setIcon(image_hover);
		});
		google.maps.event.addListener(marker, 'mouseout', function() {
			marker.setIcon(image);
		});
	}
	function centerMap(map) {
	    var bounds = new google.maps.LatLngBounds();
	    map.markers.forEach(function(marker){
	        bounds.extend({
	            lat: marker.position.lat(),
	            lng: marker.position.lng()
	        });
	    });
	    if(map.markers.length == 1){
	        map.setCenter(bounds.getCenter());
	    } else{
	        map.fitBounds(bounds);
	    }
	}
	$('#map').each(function(){
        var map = initMap($(this));
    });
</script>	

<?php get_footer(); ?>