<?php
/*
 * Template Name: News Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div class="container">	
	
	<section class="two-col grid no-top">
		
		<?php
		$args = array(
			'orderby' => 'date',
		    'order'   => 'DESC',
		    'posts_per_page' => -1
		);
		// The Query
		$query = new WP_Query( $args );
		 
		// The Loop
		if ( $query->have_posts() ) {
		    while ( $query->have_posts() ) {
		        $query->the_post();
		?>
		
    	<div class="column width-50 pull-left">
			<div class="inner post <?php if(!get_field('featured_image')) { echo 'no-image ';}?>
			
			<?php 
		    foreach((get_the_category()) as $category) {
		        echo $category->slug . " ";
		    }
		    ?>
				
			 text-center">
				<?php 
				if(get_field('external_link')) {
					$link = get_field('external_link');
				} else {
					$link = get_permalink();
				} 
				?>
				<a href="<?php echo $link;?>" class="post-thumb" <?php if(get_field('external_link')) { echo 'target="_blank"';}?>>
					<?php if(get_field('featured_image')) { ?>
					<?php $image = get_field('featured_image'); ?>
					<img src="<?php echo $image['url'];?>">
					<?php } ?>
					<div class="tag"><img src="<?php bloginfo('template_url'); ?>/img/tag.png"></div>
				</a>	
				<h3><a href="<?php echo $link;?>" <?php if(get_field('external_link')) { echo 'target="_blank"';}?>><?php the_title();?></a></h3><br>
				<?php if(get_field('show_date') == '1') { ?><p class="date"><?php echo get_the_date('F d, Y');?></p><?php } ?>
				<?php echo custom_field_excerpt(); ?>
				
				<div class="clearfix"></div>	
				<a class="link inline-block" href="<?php echo $link;?>" <?php if(get_field('external_link')) { echo 'target="_blank"';}?>>Learn More</a>
			</div>	
		</div>
		
		<?php
		    }
		} else {
		    // no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();
		?>
		
	</section>	

	
</div>

<script>
	$(document).ready(function(){
		news();
	});
</script>	

<?php get_footer(); ?>