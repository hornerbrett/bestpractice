<?php
/*
 * Template Name: Home Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div class="container">	
	
	<div class="intro text-center relative">
		<?php the_field('intro');?>	
		<?php $image = get_field('intro_image'); ?>
		<div class="top-image image portrait" data-bottom-top="transform: translateY(-50px);" data-top-bottom="transform: translateY(50px);">
			<img class="animate" src="<?php echo $image['url'];?>">
		</div>	
	</div>	

</div>
<section id="residential" class="inverse">
	<a id="residential-title" class="title animate" href="<?php echo site_url();?>/work/#residential">
		<div data-bottom-top="transform: translateY(0px);" data-top-bottom="transform: translateY(100px);">
			<?php include get_template_directory() . '/img/titles/residential.svg'; ?>
		</div>
	</a>	
	<div class="container">
		<?php
		$image = get_field('residential_project_image_1');
		$post_object = get_field('residential_project_1');
	
		$title = $post_object->post_title; //post title
		$link = get_permalink($post_object->ID); //URL
		?>
		
		<div class="left-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
			<a href="<?php echo $link;?>" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p><?php echo $title;?></p>	
					</div>	
					<img class="animate" src="<?php echo $image['url'];?>">
				</div>
			</a>	
		</div>	
		
		<?php
		$image = get_field('residential_project_image_2');
		$post_object = get_field('residential_project_2');
	
		$title = $post_object->post_title; //post title
		$link = get_permalink($post_object->ID); //URL
		?>
		
		<div class="left-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
			<a href="<?php echo $link;?>" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p><?php echo $title;?></p>	
					</div>	
					<img class="animate" src="<?php echo $image['url'];?>">
				</div>
			</a>	
		</div>	
	</div>	
	
</section>	
<section id="retail">
	
	<a id="retail-title" class="title animate" href="<?php echo site_url();?>/work/#commercial">
		<div data-bottom-top="transform: translateY(-100px);" data-top-bottom="transform: translateY(100px);">
			<?php include get_template_directory() . '/img/titles/commercial.svg'; ?>
		</div>
	</a>

	<div class="container">
		
		<?php
		$image = get_field('commercial_project_image_1_copy');
		$post_object = get_field('commercial_project_1');
	
		$title = $post_object->post_title; //post title
		$link = get_permalink($post_object->ID); //URL
		?>
		
		<div class="left-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
			<a href="<?php echo $link;?>" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p><?php echo $title;?></p>	
					</div>	
					<img class="animate" src="<?php echo $image['url'];?>">
				</div>
			</a>	
		</div>	
		
		<?php
		$image = get_field('commercial_project_image_2');
		$post_object = get_field('commercial_project_2');
	
		$title = $post_object->post_title; //post title
		$link = get_permalink($post_object->ID); //URL
		?>
		
		<div class="left-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
			<a href="<?php echo $link;?>" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p><?php echo $title;?></p>	
					</div>	
					<img class="animate" src="<?php echo $image['url'];?>">
				</div>
			</a>	
		</div>	
		
<!--
		<div class="left-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
			<a href="#" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p>Project Name Goes Here</p>	
					</div>	
					<img class="animate" src="<?php bloginfo('template_url'); ?>/img/portrait.jpg">
				</div>
			</a>	
		</div>	
		<div class="right-image item" data-bottom-top="transform: translateY(-100px);" data-top-bottom="transform: translateY(100px);">
			<a href="#" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p>Project Name Goes Here</p>	
					</div>	
					<img class="animate" src="<?php bloginfo('template_url'); ?>/img/landscape.jpg">
				</div>
			</a>
		</div>	
-->
	</div>	
</section>	

<section id="restaurant" class="inverse">
	<a id="restaurant-title" class="title animate" href="<?php echo site_url();?>/work/#restaurant">
		<div data-bottom-top="transform: translateY(-100px);" data-top-bottom="transform: translateY(100px);">
			<?php include get_template_directory() . '/img/titles/restaurant.svg'; ?>
		</div>
	</a>	
	<div class="container">
		
		<?php
		$image = get_field('restaurant_project_image_1');
		$post_object = get_field('restaurant_project_1');
	
		$title = $post_object->post_title; //post title
		$link = get_permalink($post_object->ID); //URL
		?>
		
		<div class="left-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
			<a href="<?php echo $link;?>" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p><?php echo $title;?></p>	
					</div>	
					<img class="animate" src="<?php echo $image['url'];?>">
				</div>
			</a>	
		</div>	
		
		<?php
		$image = get_field('restaurant_project_image_2');
		$post_object = get_field('restaurant_project_2');
	
		$title = $post_object->post_title; //post title
		$link = get_permalink($post_object->ID); //URL
		?>
		
		<div class="left-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
			<a href="<?php echo $link;?>" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p><?php echo $title;?></p>	
					</div>	
					<img class="animate" src="<?php echo $image['url'];?>">
				</div>
			</a>	
		</div>	
	
<!--
	<div class="left-image item" data-bottom-top="transform: translateY(-100px);" data-top-bottom="transform: translateY(100px);">
		<a href="#" class="work-thumb">
			<div class="inner">
				<div class="centered text-center">
					<p>Project Name Goes Here</p>	
				</div>
				<img class="animate" src="<?php bloginfo('template_url'); ?>/img/landscape.jpg">
			</div>
		</a>
	</div>	
	<div class="right-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
		<a href="#" class="work-thumb">
			<div class="inner">
				<div class="centered text-center">
					<p>Project Name Goes Here</p>	
				</div>
				<img class="animate" src="<?php bloginfo('template_url'); ?>/img/portrait.jpg">
			</div>
		</a>
	</div>
-->
	
	</div>	
</section>	

<section id="office">
	
	<a id="office-title" class="title animate" href="<?php echo site_url();?>/work/#office">
		<div data-bottom-top="transform: translateY(-100px);" data-top-bottom="transform: translateY(100px);">
			<?php include get_template_directory() . '/img/titles/office.svg'; ?>
		</div>
	</a>	
	
	<div class="container">
		<?php
		$image = get_field('office_project_image_1');
		$post_object = get_field('office_project_1');
	
		$title = $post_object->post_title; //post title
		$link = get_permalink($post_object->ID); //URL
		?>
		
		<div class="left-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
			<a href="<?php echo $link;?>" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p><?php echo $title;?></p>	
					</div>	
					<img class="animate" src="<?php echo $image['url'];?>">
				</div>
			</a>	
		</div>	
		
		<?php
		$image = get_field('office_project_image_2');
		$post_object = get_field('office_project_2');
	
		$title = $post_object->post_title; //post title
		$link = get_permalink($post_object->ID); //URL
		?>
		
		<div class="left-image item" data-bottom-top="transform: translateY(150px);" data-top-bottom="transform: translateY(-50px);">
			<a href="<?php echo $link;?>" class="work-thumb">
				<div class="inner">
					<div class="centered text-center">
						<p><?php echo $title;?></p>	
					</div>	
					<img class="animate" src="<?php echo $image['url'];?>">
				</div>
			</a>	
		</div>	
	</div>	
	
</section>	

<div class="container">
	<section class="text-image">
		<div class="left-text text-center width-50 pull-left">
			<div class="inner pull-right text">
				<h1><?php the_field('studio_title');?></h1>
				<?php the_field('studio_text');?>
				<a class="link" href="<?php echo site_url(); ?>/studio">Learn More</a>
	
			</div>	
		</div>	
		<div class="right-image">
			<?php $image = get_field('studio_image'); ?>
			<img class="animate" src="<?php echo $image['url'];?>">
		</div>	
	</section>	
	
	<section class="two-col">
		
		<div class="news-title pull-right">
			<h1 class="text-center">News</h1>
		</div>	
		
		<?php 
		$post_objects = get_field('news');
		?>
		
		<?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
        	<?php setup_postdata($post); ?>
        		
	    	<div class="column width-50 pull-left">
				<div class="inner post <?php if(!get_field('featured_image')) { echo 'no-image ';}?>
				
				<?php 
			    foreach((get_the_category()) as $category) {
			        echo $category->slug . " ";
			    }
			    ?>
					
				 text-center">
					<?php 
					if(get_field('external_link')) {
						$link = get_field('external_link');
					} else {
						$link = get_permalink();
					} 
					?>
					<a href="<?php echo $link;?>" class="post-thumb" <?php if(get_field('external_link')) { echo 'target="_blank"';}?>>
						<?php if(get_field('featured_image')) { ?>
						<?php $image = get_field('featured_image'); ?>
						<img src="<?php echo $image['url'];?>">
						<?php } ?>
						<div class="tag"><img src="<?php bloginfo('template_url'); ?>/img/tag.png"></div>
					</a>	
					<h3><a href="<?php echo $link;?>" <?php if(get_field('external_link')) { echo 'target="_blank"';}?>><?php the_title();?></a></h3><br>
					<?php if(get_field('show_date') == '1') { ?><p class="date"><?php echo get_the_date('F d, Y');?></p><?php } ?>
					
					<p><?php echo custom_field_excerpt(); ?></p>
					
					<div class="clearfix"></div>	
					<a class="link inline-block" href="<?php echo $link;?>" <?php if(get_field('external_link')) { echo 'target="_blank"';}?>>Learn More</a>
				</div>	
			</div>
		
		<?php endforeach; ?>
		
		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

	</section>	
	
</div>

<?php get_footer(); ?>